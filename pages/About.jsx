import Router from "next/router";

export default function About() {
  const linkClickHendler = () => {
    Router.push("/");
  };
  return (
    <>
      <h1>Hello About</h1>
      <button onClick={linkClickHendler}>Go to back Home Page</button>
      <button onClick={() => Router.push("/Posts")}>Go to posts</button>
    </>
  );
}
