import Link from "next/link";
import Head from "next/head";

export default function Index() {
  return (
    <>
      <Head>
        <meta charset="utf-8" />
        <meta name="keywords" content="next javascript react" />
        <title>Home</title>
      </Head>
      <h1>Hello Nextjs</h1>
      <Link href={"/About"}>
        <a>About</a>
      </Link>
    </>
  );
}
